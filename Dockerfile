FROM ubuntu:latest
# Reamake https://hub.docker.com/r/chrisdaish/google-chrome/~/dockerfile/
# https://github.com/chrisdaish/docker-chrome
MAINTAINER Chris Daish <chrisdaish@gmail.com>

ENV DEBIAN_FRONTEND noninteractive

RUN useradd -m google-chrome
RUN echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/chrome.list
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 6494C6D6997C215E
RUN apt update
RUN apt install -y --no-install-recommends ca-certificates \
	gconf-service \
	hicolor-icon-theme \
	libappindicator1 \
	libasound2 \
	libcurl3 \
	libexif-dev \
	libgconf-2-4 \
	libgl1-mesa-dri \
	libgl1-mesa-glx \
	libnspr4 \
	libnss3 \
	libpango1.0-0 \
	libv4l-0 \
	libxss1 \
	libxtst6 \
	xdg-utils \
	google-chrome-stable
RUN rm -rf /var/lib/apt/lists/*
RUN rm -f /var/cache/apt/archives/*.deb

COPY start-google-chrome.sh /usr/local/bin/

ENTRYPOINT ["/usr/local/bin/start-google-chrome.sh"]

# Start:
# docker run  -v $HOME/Downloads:/home/google-chrome/Downloads:rw \
#	-v /tmp/.X11-unix:/tmp/.X11-unix \
#	-v /dev/snd:/dev/snd \
#	-v /dev/shm:/dev/shm \
#	--privileged \
#	-e uid=$(id -u) \
#	-e gid=$(id -g) \
#	-e DISPLAY=unix$DISPLAY \
#	--rm \
#	--name google-chrome \
#	chrome